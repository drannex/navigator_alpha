const { promisify } = require('util');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const passport = require('passport');
const User = require('../models/User');
const Document = require('../models/Document');

/**
 * GET /
 * Home page.
 */
exports.index = (req, res) => {
  if (!req.user) {
    res.render('home', {
      title: 'Home',
    });
  } else {
    return res.redirect('/notes');
  }
};

/**
 * GET /notes
 * Notes
 */
exports.getNotesPage = (req, res) => {
  res.render('admin/files/files.njk', {
    title: 'Notes',
  });
};

