const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');
const shortid = require('shortid');
const sanitizeHtml = require('sanitize-html');


const documentSchema = new mongoose.Schema({
    user_id: {type: String, required: true},
    document_id: { type: String, default: shortid.generate() },
    title: String,
    content: String,
    type: String,
    tags: { type: Array, default: [] },
    archived: { type: String, default: 'false'},

    meta: {
        fileType: String,
        fileSize: String,
        fileName: String,
        fileLocation: String,
        fileExtension: String,
        other: Array
    },

    privacy: {
        published: { type: String, default: false },
        userAccessList: Array,
        private_id: String,
        expires: String,
    }

}, { timestamps: true });

documentSchema.index({ '$**': 'text' });

const Document = mongoose.model('Document', documentSchema);

module.exports = Document;

