Vue.filter('striphtml', function (value) {
  var div = document.createElement("div");
  div.innerHTML = value;
  var text = div.textContent || div.innerText || "";
  return text;
});

Vue.filter('cut', function (value, count) {
  return value.length < count ?value : value.slice(0, count)
})

Vue.filter('truncate', function (value, count) {
  return value.length < count ? value : value.slice(0, count) + '...';
})